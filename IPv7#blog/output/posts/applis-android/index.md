<!--
.. title: Applications Android
.. slug: applis-android
.. date: 2020-06-29 22:58:18 UTC+02:00
.. tags: vie privée, FOSS, F-Droid, Android, privacy
.. category: Android
.. link: 
.. description: 
.. type: text
-->

<!--
TODO : ajout flag rouge quand app propriétaire
-->
Hello World 🙃

Premier post de ce blog pour lister les applications Android que j'utilise, après plusieurs années de tests et de recherches continues. N'ayant pas encore rédigé la page de présentation de ce blog, je précise que j'essaie d'orienter mon utilisation des outils numériques dans une optique de logiciel libre (FOSS : Free & Open Source Software) et de vie privée.

Allons-y donc :

<style>
.img-legende{
  height : 3ex;
  display: inline-block;
  margin:0;
}

.img-caracteristiques{
  height : 3ex;
  display: inline-block;
}
</style>

| **Logo** | **Légende** |
| :-: | :-: |
| <img src="/images/hashtag_470x422.png" alt="root nécéssaire" class="img-caracteristiques"> | root nécéssaire |
| <img src="/images/hashtag_tilda_470x422.png" alt="root optionnel" class="img-caracteristiques"> | root  optionnel |
| <img src="/images/hashtag_barre_470x422.png" alt="root inutile" class="img-caracteristiques"> | root inutile |
| <img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"> | provenance F-Droid<br>(store Android d'applications libres et respectueuses de la vie privée) |
| <img src="/images/logo_github_512x512.png" alt="logo GitHub" class="img-caracteristiques"> | provenance GitHub |
| <img src="/images/logo_PlayStore_512x512.png" alt="logo Google Play Store" class="img-caracteristiques"> | provenance Google Play Store |


### Sécurité / Privacy

| **Application** | **Présentation** | **Commentaire** |
| :------: | ------ | ------ |
| [Adaway](https://f-droid.org/fr/packages/org.adaway/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_470x422.png" alt="root nécéssaire" class="img-caracteristiques"></div>| Bloqueur de publicités/tracking | blocage des publicités à l'aide d'un résolveur DNS local et de listes de filtrage |
| [Blokada](https://f-droid.org/packages/org.blokada.alarm/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_barre_470x422.png" alt="root inutile" class="img-caracteristiques"></div>| Bloqueur de publicités/tracking | Alternative "non-root" à Adaway, fonctionne en émulant un VPN local.<br>⚠️ ne pas utiliser la version Play Store car incomplète ! |
| [AFWall+](https://f-droid.org/fr/packages/dev.ukanth.ufirewall/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_470x422.png" alt="root nécéssaire" class="img-caracteristiques"></div>| Pare-feu | pour définir de manière plus fine les autorisations réseaux pour chaque application |
| [andOTP](https://f-droid.org/fr/packages/org.shadowice.flocke.andotp/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_barre_470x422.png" alt="root inutile" class="img-caracteristiques"></div>| client TOTP | <ul><li>problème perso de backups corrompues</li><li>[faiblesses cryptographiques ?](https://www.reddit.com/r/privacytoolsIO/comments/fmjtzn/fortitoken_mobile_vs_aegis_vs_andotp/fl4pljk)</li></ul>|
| [Aegis](https://f-droid.org/fr/packages/com.beemdevelopment.aegis/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_barre_470x422.png" alt="root inutile" class="img-caracteristiques"></div>| client TOTP supportant l'Android Keystore | <ul><li>déverrouillage possible par empreintes digitales</li><li>pas de récupération automatique d'icônes</li><li>ni de preset d'icônes</li></ul> |
| [BackgroundRestrictor](https://f-droid.org/fr/packages/com.pavelsikun.runinbackgroundpermissionsetter/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_470x422.png" alt="root nécéssaire" class="img-caracteristiques"></div>|  |
* SnoopSnitch


### Utilitaires

| **Application** | **Présentation** | **Commentaire** |
| :------: | ------ | ------ |
| [Amaze](https://github.com/TeamAmaze/AmazeFileManager) <div><img src="/images/logo_github_512x512.png" alt="logo Github" class="img-caracteristiques"><img src="/images/hashtag_tilda_470x422.png" alt="root optionnel" class="img-caracteristiques"></div>| Explorateur de fichiers | la version [F-Droid](https://f-droid.org/fr/packages/com.amaze.filemanager/) n'est plus à jour depuis 2017 ([issue ouverte](https://github.com/TeamAmaze/AmazeFileManager/issues/1886)) |
| [Aurora Droid](https://f-droid.org/fr/packages/com.aurora.adroid/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_tilda_470x422.png" alt="root optionnel" class="img-caracteristiques"></div>| Client F-Droid, design amélioré | Pour améliorer l'intégration au système, installer <a href="https://gitlab.com/AuroraOSS/AuroraServices" target="_blank">Aurora Services</a> |
| [Aurora Store](https://f-droid.org/fr/packages/com.aurora.store/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_tilda_470x422.png" alt="root optionnel" class="img-caracteristiques"></div>| Client alternatif Play Store | Pour améliorer l'intégration au système, installer <a href="https://gitlab.com/AuroraOSS/AuroraServices" target="_blank">Aurora Services</a> |
| [Binary Eye](https://f-droid.org/fr/packages/de.markusfisch.android.binaryeye/) <div><img src="/images/logo_F-Droid.png" alt="logo F-Droid" class="img-caracteristiques"><img src="/images/hashtag_470x422.png" alt="root inutile" class="img-caracteristiques"></div>| Scanner QR Codes, Flash Codes et Barcodes |  |
* [BusyBox](https://f-droid.org/fr/packages/ru.meefik.busybox/)
* [Call Recorder](https://f-droid.org/fr/packages/com.github.axet.callrecorder/)
* [ClipboardCleaner](https://f-droid.org/fr/packages/io.github.deweyreed.clipboardcleaner/)
* [ConnectBot](https://f-droid.org/fr/packages/org.connectbot/)
* TermBot
* [DAVx5](https://f-droid.org/fr/packages/at.bitfire.davdroid/)
* [ICSx5](https://f-droid.org/fr/packages/at.bitfire.icsdroid/)
* [Déjà Vu Location Service](https://f-droid.org/fr/packages/org.fitchfamily.android.dejavu/)
* FreshRSS
* [Gadgetbridge](https://f-droid.org/fr/packages/nodomain.freeyourgadget.gadgetbridge/)
* [Hash Droid](https://f-droid.org/fr/packages/com.hobbyone.HashDroid/)
* [Identiconizer!](https://f-droid.org/fr/packages/com.germainz.identiconizer/)
* [K-9 Mail](https://f-droid.org/fr/packages/com.fsck.k9/)
* [Frost for Facebook](https://f-droid.org/fr/packages/com.pitchedapps.frost/)
* Tusky
* [LibreTorrent](https://f-droid.org/fr/packages/org.proninyaroslav.libretorrent/)
* [Loyalty Card Keychain](https://f-droid.org/fr/packages/protect.card_locker/)
* [Net Monitor](https://f-droid.org/fr/packages/org.secuso.privacyfriendlynetmonitor/)
* [Nextcloud](https://f-droid.org/fr/packages/com.nextcloud.client/)
* [oandbackup](https://f-droid.org/fr/packages/dk.jens.backup/)
* [OpenFoodFacts](https://f-droid.org/fr/packages/openfoodfacts.github.scrachx.openfood/)
* [Oscilloscope](https://f-droid.org/fr/packages/org.billthefarmer.scope/)
* Signal Generator
* [RF Analyzer](https://f-droid.org/fr/packages/com.mantz_it.rfanalyzer/)
* WiFiAnalyzer
* [OsmAnd+](https://f-droid.org/fr/packages/net.osmand.plus/)
* Transportr
* [Pi-hole Droid](https://f-droid.org/fr/packages/friimaind.piholedroid/)
* [FlutterHole](https://f-droid.org/fr/packages/sterrenburg.github.flutterhole/)
* [Scrambled Exif](https://f-droid.org/fr/packages/com.jarsilio.android.scrambledeggsif/)
* ykDroid
* Share to Computer
* Simple Calendar Pro
* SysLog
* Silence
* TV KILL
* Termux
* Todo Agenda
* Tutanota
* WifiKeyShare


### Multimédia

* AntennaPod
* [NewPipe](https://f-droid.org/fr/packages/org.schabi.newpipe/)
* [RadioDroid](https://f-droid.org/fr/packages/net.programmierecke.radiodroid2/)
* VLC


### Web

* [Fennec F-Droid](https://f-droid.org/fr/packages/org.mozilla.fennec_fdroid/)
* [Firefox Klar](https://f-droid.org/fr/packages/org.mozilla.klar/)
* Tor Browser


# Bureautique

* [Document Viewer](https://f-droid.org/fr/packages/org.sufficientlysecure.viewer/)
* [Editor](https://f-droid.org/fr/packages/org.billthefarmer.editor/) (de texte)
* Collabora
* muPDF
* [LibreOffice Viewer](https://f-droid.org/fr/packages/org.documentfoundation.libreoffice/)









### Non-FOSS

* Aliexpress
* Amazon Shopping
* Anydesk
* Authy (à dégager)
* Automate (à remplacer par Easer ?)
* Bass Booster (équivalent? AudioFX gère les basses ? Module Magisk ?)

